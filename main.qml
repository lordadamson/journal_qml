import QtQuick 2.12
import QtQuick.Window 2.12
import QtGraphicalEffects 1.12

Item {
    id: root

    visible: true

    Sidebar {
        id: sidebar
        x: 0
        y: 0
        width: 30 * Screen.pixelDensity
        height: parent.height
    }

    MyCanvas {
        anchors.left: sidebar.right
        width: parent.width
        height: parent.height
    }
}
