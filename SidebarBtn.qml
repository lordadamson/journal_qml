import QtQuick 2.0
import QtGraphicalEffects 1.12

Item {
    property alias color: btn.color

    Rectangle {
        anchors.fill: parent
        id: btn
    }

    DropShadow {
        anchors.fill: btn
        radius: 8.0
        samples: 17
        color: "#80000000"
        source: btn
    }
}
