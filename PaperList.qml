import QtQuick 2.0

ListView {
    ListModel {
        id: m
        ListElement {
          name: "Bill Smith"
        }
        ListElement {
          name: "John Brown"
        }
        ListElement {
          name: "Sam Wise"
        }
        ListElement {
          name: "mayada"
        }
    }

    model: m
    delegate: Rectangle {
        height: 20
        width: parent.width

        color: "#ddd"
        Text {
            anchors.centerIn: parent
            text: name
        }
    }
}
