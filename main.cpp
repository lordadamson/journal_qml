#include <QGuiApplication>
#include <QQuickView>
#include <QQmlApplicationEngine>
#include <QFileSystemWatcher>
#include <QFile>
#include <QDebug>

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QGuiApplication app(argc, argv);

	QQuickView qview;

	QFileSystemWatcher watcher;
	watcher.addPaths({
						  SOURCE_DIR "main.qml",
						  SOURCE_DIR "MyCanvas.qml",
						  SOURCE_DIR "PaperList.qml",
						  SOURCE_DIR "Sidebar.qml",
						  SOURCE_DIR "SidebarBtn.qml"
					  });

	const QUrl url(QStringLiteral(SOURCE_DIR "main.qml"));

	qview.setResizeMode(QQuickView::ResizeMode::SizeRootObjectToView);
	qview.setSource(url);
	qview.show();

	QObject::connect(&watcher, &QFileSystemWatcher::fileChanged, &qview,
		[&qview, &url, &watcher] (const QString &path) {
			qview.engine()->clearComponentCache();
			qview.setSource(url);

			bool in_watcher = watcher.files().contains(path);
			if((!in_watcher) && QFile::exists(path))
			{
				watcher.addPath(path);
			}
	});

	return app.exec();
}
