import QtQuick 2.0
import QtQuick.Window 2.12

Item {
    Rectangle {
        id: sidebar
        color: "#333"
        anchors.fill: parent
        readonly property int btn_count: 3

        SidebarBtn {
            id: btn1
            x: parent.x
            width: parent.width / parent.btn_count
            height: width
            color: "#3f9384"
        }

        SidebarBtn {
            id: btn2
            anchors.left: btn1.right
            width: parent.width / parent.btn_count
            height: width
            color: "#e36900"
        }

        SidebarBtn {
            id: btn3
            anchors.left: btn2.right
            width: parent.width / parent.btn_count
            height: width
            color: "#fad768"
        }

        PaperList {
            anchors.top: btn1.bottom
            width: parent.width
            height: parent.height
        }
    }
}
