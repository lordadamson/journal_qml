import QtQuick 2.0

Rectangle {
    color: "#eee"

    Canvas {
        id: canvas
        anchors.fill: parent

        property bool drawing: false
        property int x1: -1
        property int y1: -1
        property int x2: -1
        property int y2: -1

        onPaint: {
            var ctx = canvas.getContext('2d')
            ctx.moveTo(x1, y1)
            ctx.lineTo(x2, y2)
            ctx.lineWidth = 4
            ctx.strokeStyle = "#468966"
            ctx.stroke()
            canvas.x1 = canvas.x2
            canvas.y1 = canvas.y2
        }

        MouseArea {
            anchors.fill: parent

            onPressed: {
                canvas.drawing = true;
                canvas.x1 = mouseX
                canvas.y1 = mouseY
                canvas.x2 = mouseX
                canvas.y2 = mouseY
            }

            onPositionChanged: {
                if(!canvas.drawing)
                {
                    return
                }

                canvas.x2 = mouseX
                canvas.y2 = mouseY

                canvas.requestPaint()
            }

            onReleased: {
                canvas.drawing = false;
            }
        }
    }
}
